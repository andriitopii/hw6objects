// Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об'єкту
// Методи об'єкту це властивість обєкту значенням якої є функція.
// 2. Який тип даних може мати значення властивості об'єкта?
// будь який доступний тип даних. Обєкт фкуеція масив число текст будь-що
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
//Це означає що в памяті як на приклад з примітивними типами даних не зберігається конкретне значення, 
// а зберінається посилання на ці дані. Тому що обєкт це складний тип даних в якому можуть бути безліч примітивних типів даних які 
// будуть добавлятися або видалятися по ходу роботи програми. Наприклад 
// const obj = { name: 'asss'} в зміггій obj насправді зберігається посилання(адрес в оперативній памяті) на дані об'єкта.
// тому наприклад коли ми запишемо так:
// const obj = {name: 'Alla'}; conts objNew = obj; то в змінну objNew потрпить посилання на обєк а не сам обєкт

// Практичні завдання
// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
// 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
// 3.Опціональне. Завдання:
// Реалізувати повне клонування об'єкта.
// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.

// TASK 1
const product = {
    name: "Mac Book Air 13'",
    price: 3000,
    _discount: 200,
    set discount(val){if(val > this.price){ console.log('Помилка')} else this._discount = val},
    get discount(){ return this._discount},
    fullPrice(){
         
        return this.price - this._discount;
    }
};
product.discount = 500;
// console.log(product.fullPrice())

// TASK 2

const user = {
}
let userName;
let userAge;
const userHello = (obj) => {
    do{
        userName = prompt("Введіть ім'я")
        
    } while(userName === '' || userName === null || !isNaN(userName));
    obj.name = userName;
    do{
        userAge = prompt("Введіть Вік")

    } while(userAge === '' || userAge === null || isNaN(userAge))
    obj.age = Number(userAge);
    return (`Привіт, ${obj.name}. Мені ${obj.age} років` )
}
// alert(userHello(user));
// console.log(user)

//TASK 3
  const objPersonal = {
    name: 'Andrii',
    age: 23,
    phone: 123123123,
    experiance: {
        job: "APPLE CORPORATION",
        education: {name: {holo: [1,2,34,444]}}
    }
    }

  
function clonedObj(obj){
    const cloned = {};
    for (let i in obj) cloned[i] = typeof cloned[i] == 'object' ? clonedObj(obj[i]) : obj[i]
    return cloned;
}

const newObjectCloned = clonedObj(objPersonal);
newObjectCloned.name = 'Igor';
console.log(objPersonal)
console.log(newObjectCloned)